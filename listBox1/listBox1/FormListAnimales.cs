﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace listBox1
{
    public partial class FormListAnimales : Form
    {
        public FormListAnimales()
        {
            InitializeComponent();
        }

        
        //Botón añadir
        private void btnAnnadir_Click(object sender, EventArgs e)
        {
            if(tbAnnadirAnimal.TextLength > 0)
            {
                lb_Animales.Items.Add(tbAnnadirAnimal.Text);
                tbAnnadirAnimal.Clear();
                MessageBox.Show("Insercción realizada correctamente","Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("debe introducir prmero un animal", "ERROR");
            }
        }

        
        // botón 'Aceptar': para mostrar todos los animales que has seleccionado
        private void btnAceptar_Click(object sender, EventArgs e)
        {

        }
    }
}
