﻿namespace listBox1
{
    partial class FormListAnimales
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.lb_Animales = new System.Windows.Forms.ListBox();
            this.btnAceptar = new System.Windows.Forms.Button();
            this.btnAnnadir = new System.Windows.Forms.Button();
            this.btnBorrar = new System.Windows.Forms.Button();
            this.tbAnnadirAnimal = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lb_Animales
            // 
            this.lb_Animales.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lb_Animales.FormattingEnabled = true;
            this.lb_Animales.ItemHeight = 22;
            this.lb_Animales.Items.AddRange(new object[] {
            "Perro",
            "Capivara",
            "Castor",
            "Nutria"});
            this.lb_Animales.Location = new System.Drawing.Point(29, 23);
            this.lb_Animales.Name = "lb_Animales";
            this.lb_Animales.SelectionMode = System.Windows.Forms.SelectionMode.MultiSimple;
            this.lb_Animales.Size = new System.Drawing.Size(184, 268);
            this.lb_Animales.TabIndex = 0;
            // 
            // btnAceptar
            // 
            this.btnAceptar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAceptar.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAceptar.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnAceptar.Location = new System.Drawing.Point(266, 23);
            this.btnAceptar.Name = "btnAceptar";
            this.btnAceptar.Size = new System.Drawing.Size(140, 33);
            this.btnAceptar.TabIndex = 1;
            this.btnAceptar.Text = "ACEPTAR";
            this.btnAceptar.UseVisualStyleBackColor = true;
            this.btnAceptar.Click += new System.EventHandler(this.btnAceptar_Click);
            // 
            // btnAnnadir
            // 
            this.btnAnnadir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnadir.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnadir.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnAnnadir.Location = new System.Drawing.Point(266, 219);
            this.btnAnnadir.Name = "btnAnnadir";
            this.btnAnnadir.Size = new System.Drawing.Size(140, 33);
            this.btnAnnadir.TabIndex = 2;
            this.btnAnnadir.Text = "AÑADIR";
            this.btnAnnadir.UseVisualStyleBackColor = true;
            this.btnAnnadir.Click += new System.EventHandler(this.btnAnnadir_Click);
            // 
            // btnBorrar
            // 
            this.btnBorrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBorrar.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBorrar.ForeColor = System.Drawing.Color.MidnightBlue;
            this.btnBorrar.Location = new System.Drawing.Point(266, 258);
            this.btnBorrar.Name = "btnBorrar";
            this.btnBorrar.Size = new System.Drawing.Size(140, 33);
            this.btnBorrar.TabIndex = 3;
            this.btnBorrar.Text = "BORRAR";
            this.btnBorrar.UseVisualStyleBackColor = true;
            // 
            // tbAnnadirAnimal
            // 
            this.tbAnnadirAnimal.Font = new System.Drawing.Font("Microsoft YaHei", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbAnnadirAnimal.Location = new System.Drawing.Point(267, 156);
            this.tbAnnadirAnimal.Multiline = true;
            this.tbAnnadirAnimal.Name = "tbAnnadirAnimal";
            this.tbAnnadirAnimal.Size = new System.Drawing.Size(139, 57);
            this.tbAnnadirAnimal.TabIndex = 4;
            // 
            // FormListAnimales
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(428, 315);
            this.Controls.Add(this.tbAnnadirAnimal);
            this.Controls.Add(this.btnBorrar);
            this.Controls.Add(this.btnAnnadir);
            this.Controls.Add(this.btnAceptar);
            this.Controls.Add(this.lb_Animales);
            this.Name = "FormListAnimales";
            this.Text = "Listado de Animales";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lb_Animales;
        private System.Windows.Forms.Button btnAceptar;
        private System.Windows.Forms.Button btnAnnadir;
        private System.Windows.Forms.Button btnBorrar;
        private System.Windows.Forms.TextBox tbAnnadirAnimal;
    }
}

